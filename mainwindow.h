﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QPushButton>
#include <QProgressBar>
#include <QTextBrowser>
#include <QVBoxLayout>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void btnUpdate();
    void progressChanged(int num);
    void dealTimeOut();
    void setParam(QString &dir, QString &newVersionorder, QString &newVersionno, QString &updateInfo);
    void closeEvent(QCloseEvent *);
    void processExits();
    void dealConfig();
private:
    QVBoxLayout *layout;
    QTimer *timer;
    QPushButton * pushBtn;
    QProgressBar * progressBar;
    QTextBrowser * textBrowser;
    QString m_fileDir;
    QString m_newVersionOrder;
    QString m_newVersionNo;
    QString m_updateInfo;
    const QString GISDATA = "../../GISDATA/";
};
#endif // MAINWINDOW_H
