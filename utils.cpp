﻿#include "utils.h"
#include <QDir>
#include <QDebug>
#include <QSettings>

//源文件文件路径，目的文件路径，文件存在是否覆盖
bool utils::copyFile(QString srcPath, QString dstPath, bool coverFileIfExist)
{
    srcPath.replace("\\", "/");
    dstPath.replace("\\", "/");
    if (srcPath == dstPath) {
        return true;
    }

    if (!QFile::exists(srcPath)) {  //源文件不存在
        return false;
    }

    if (QFile::exists(dstPath)) {
        if (coverFileIfExist) {
            QFile::remove(dstPath);
        }
    }

    int temp = dstPath.lastIndexOf("/");
    QString filePath = dstPath.left(temp + 1);
    QDir* dir = new QDir();
    //如果路径不存在，则创建
    if(!dir->exists(filePath)){
        if(dir->mkpath(filePath)){
            if (!QFile::copy(srcPath, dstPath)){
                return false;
            }
        }else{
            return false;
        }
    }else {
        if (!QFile::copy(srcPath, dstPath)){
            return false;
        }
    }

    return true;
}

//源文件目录路径，目的文件目录，文件存在是否覆盖
bool utils::copyDir(const QString &source,const QString &destination, bool override)
{

        QDir directory(source);
        if (!directory.exists())
        {
            return false;
        }

        QString srcPath = QDir::toNativeSeparators(source);
        srcPath = QFileInfo(srcPath).absoluteFilePath();
        if (!srcPath.endsWith(QDir::separator()))
            srcPath += QDir::separator();

        QString dstPath = QDir::toNativeSeparators(destination);
        dstPath = QFileInfo(dstPath).absoluteFilePath();
        if (!dstPath.endsWith(QDir::separator()))
            dstPath += QDir::separator();

//        std::cerr<<"dstPath="<<dstPath.toStdString()<< QFileInfo(dstPath).absoluteFilePath().toStdString();
        bool error = false;

        QStringList fileNames = directory.entryList(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::Hidden);

        for (QStringList::size_type i=0; i != fileNames.size(); ++i)
        {
            QString fileName = fileNames.at(i);
            QString srcFilePath = srcPath + fileName;
            QString dstFilePath = dstPath + fileName;
            QFileInfo fileInfo(srcFilePath);
            if (fileInfo.isFile() || fileInfo.isSymLink())
            {
                bool sds = copyFile(srcFilePath,dstFilePath,override);
//                qDebug()<<"copy-->"<<sds<<srcFilePath<<dstFilePath;
            }
            else if (fileInfo.isDir())
            {
                QDir dstDir(dstFilePath);
                dstDir.mkpath(dstFilePath);
                if (!copyDir(srcFilePath, dstFilePath, override))
                {
                    error = true;
                }
            }
        }
        return !error;
}
void utils::writeValue(QString filePath, QString titleName, QString str)
{
    QSettings setFile(filePath,QSettings::IniFormat);

    if (str.contains("."))
    {
        int lastIndex = str.lastIndexOf("/");
        str = str.left(lastIndex);
    }
    setFile.setValue(titleName, str);
}

