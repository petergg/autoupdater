﻿#include "mainwindow.h"
#include "utils.h"
#include <QDebug>
#include <QProcess>
#include <QDateTime>
#include <QDir>
#include <QApplication>
#include <QSettings>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{

    setWindowTitle("在线更新");
    resize(340, 400);
    progressBar = new QProgressBar();
    progressBar->setGeometry(20,340,300,30);
    progressBar->setMinimum(0);
    progressBar->setMaximum(100);
    progressBar->setOrientation(Qt::Horizontal);  // 水平方向
    progressBar->setAlignment(Qt::AlignRight | Qt::AlignVCenter);  // 对齐方式
    connect(progressBar,&QProgressBar::valueChanged,this,&MainWindow::progressChanged);
    textBrowser = new QTextBrowser();
    textBrowser->setGeometry(20,20,300,300);
    pushBtn = new QPushButton(tr("立即更新"));
    pushBtn->setGeometry(20,340,80,30);
    connect(pushBtn,&QPushButton::clicked,this,&MainWindow::btnUpdate);
    layout = new QVBoxLayout();
    layout->addWidget(textBrowser);
    layout->addWidget(progressBar);
    layout->addWidget(pushBtn);
    layout->setMargin(20);//设置外边距
    layout->setSpacing(20);//设置内边距
    QWidget *widget = new QWidget();
    widget->setLayout(layout);
    this->setCentralWidget(widget);
}
void MainWindow::closeEvent(QCloseEvent *e){
    processExits();
}

void MainWindow::processExits(){
    if(!m_fileDir.isEmpty()){
        QDir dir;
        dir.setPath(m_fileDir);
        if(dir.exists()){
            dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
//            dir.setSorting(QDir::Name);
            if(dir.entryList().size()==2){
                dir.removeRecursively();
            }
        }
    }
}
void MainWindow::dealConfig(){
    QString filePath = GISDATA+"softupdate/softversion.ini";
    utils::writeValue(filePath,"version.versionorder",m_newVersionOrder);
    utils::writeValue(filePath,"version.versionno",m_newVersionNo);
    utils::writeValue(filePath,"version.updateInfo",m_updateInfo);
    QFile file(GISDATA+"softupdate/update.log");//此路径下没有就会自己创建一个
    bool ok = file.open(QIODevice::ReadWrite | QIODevice::Append);
        // 如果文件没有被占用可以打开
        if(ok){
            // 输出debug信息
            qDebug() << "write";
            // 创建stream
            QTextStream txtOutput(&file);
            // 在stream追加数据，并换行
            QDateTime current_date_time =QDateTime::currentDateTime();
            QString current_date =current_date_time.toString("yyyy.MM.dd hh:mm:ss.zzz  ddd");
            txtOutput << "-------------" << endl;
            txtOutput << current_date << endl;
            txtOutput << m_newVersionOrder << endl;
            txtOutput << m_newVersionNo << endl;
            txtOutput << m_updateInfo << endl;
            txtOutput << "-------------" << endl;
        }
        // 关闭文件, 保存数据
        file.close();
}
MainWindow::~MainWindow()
{
    qDebug()<<"程序析构！！！";
    processExits();
}

void MainWindow::btnUpdate()
{
    pushBtn->setEnabled(false);
    disconnect(pushBtn,&QPushButton::clicked,this,&MainWindow::btnUpdate);
    pushBtn->setText("正在更新...");
//    QString curDir = qApp->applicationDirPath();
//    qDebug()<<"curDir="<<curDir;
    bool cp2 = utils::copyDir(m_fileDir+"/GISDATA",GISDATA+"../GISDATA");
    bool cp1 = utils::copyDir(m_fileDir+"/bin",GISDATA+"../bin");
    if(cp1 && cp2){
        //  更改配置文件 写入升级日志
        dealConfig();

        //1、创建定时器对象
        timer = new QTimer(this);
        //2、建立定时器信号和槽，timeout是定时器，到时间会发送的信号；
        connect(timer,SIGNAL(timeout()),this,SLOT(dealTimeOut()));
        //3、定时器开始
        timer->start(100);//毫秒为单位，每100毫秒执行一次dealtimeout
        //  删除更新源文件
        processExits();
    }

}
void MainWindow::progressChanged(int num){
    qDebug()<<num;
    if(num==100){
        timer->stop();//定时器停止
        pushBtn->setText("更新完成，重启应用！");
        pushBtn->setEnabled(true);
        connect(pushBtn,&QPushButton::clicked,[=]{
            QString str = "./Hkrht.exe";
            qApp->quit();
            QProcess::startDetached(str);
        });
    }
}

void MainWindow::dealTimeOut(){
    static int cnt = 0;
    cnt+=10;
    progressBar->setFormat(QString("当前进度为：%1%").arg(cnt));
    progressBar->setValue(cnt);

}
void MainWindow::setParam(QString &dir, QString &newVersionorder, QString &newVersionno, QString &updateInfo){
    QString textInfo = QString("<h2 align=center >发现新版本</h2>\
            <br><font color=red size=5>%1</font>\
            <P fontsize=5  style='white-space: pre-line;margin:10;'>更新日志：\n%2</P>").arg(newVersionno).arg(updateInfo);
    textBrowser->setText(textInfo);
    m_fileDir = dir;
    m_newVersionOrder = newVersionorder;
    m_newVersionNo = newVersionno;
    m_updateInfo = updateInfo;
}
