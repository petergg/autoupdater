﻿#ifndef UTILS_H
#define UTILS_H
#include <QString>

class utils
{
public:
    //文件拷贝
    static bool copyFile(QString srcPath, QString dstPath, bool coverFileIfExist=true);
    //文件夹拷贝
    static bool copyDir(const QString &source,const QString &destination, bool override=true);
    //配置文件写入
    static void writeValue(QString filePath, QString titleName, QString str);
};

#endif // UTILS_H
