﻿#include "mainwindow.h"
#include "logger/Logger.h"
#include <QApplication>
#include <QTextCodec>
#include <QErrorMessage>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF8"));
    Logger::initLog();
    if(argc==5){
        qDebug()<<"当前软件的参数个数："<<argc<<endl;
        QTextCodec* pTextCodec = QTextCodec::codecForName("System");
        assert(pTextCodec != nullptr);
        QString dir = pTextCodec->toUnicode(argv[1]);
        QString newVersionorder = pTextCodec->toUnicode(argv[2]);
        QString newVersionno = pTextCodec->toUnicode(argv[3]);
        QString updateInfo = pTextCodec->toUnicode(argv[4]);
        qDebug()<<"dir=："<<dir<<"newVersionorder"<<newVersionorder<<"newVersionno=："<<newVersionno<<"updateInfo=："<<updateInfo<<endl;
        MainWindow *w = new MainWindow;
        w->setParam(dir,newVersionorder,newVersionno,updateInfo);
        w->show();
    }else{
        qDebug()<<"当前软件的参数个数错误："<<argc<<endl;
        QErrorMessage *dialog = new QErrorMessage();
        dialog->setWindowTitle("错误警告");
        dialog->showMessage("更新系统出了问题！请联系管理员！");
    }
//    for(int i=0;i<argc;i++){
//        QTextCodec* pTextCodec = QTextCodec::codecForName("System");
//        assert(pTextCodec != nullptr);
//        QString dir = pTextCodec->toUnicode(argv[i]);
//        qDebug()<<"当前软件的参数："<<dir<<endl;
//    }

//    MainWindow w;
//    QString dir = "P:/product/ht_master/GISDATA/softupdate/updatefile/1e39d7a3-73ce-4deb-9e26-b68005bf64e3";
//    QString newVersionorder = "1";
//    QString newVersionno = "2.0.0901";
//    QString updateInfo = "1.更新系统稳定性\n2.更新系统安全性\n3.更新系统流畅性";
//    w.setParam(dir,newVersionorder,newVersionno,updateInfo);
//    w.show();
    return a.exec();
}
